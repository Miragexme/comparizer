import os
import datetime
from copy import deepcopy
from shutil import copyfile

import time

from PIL import Image
from comtypes.safearray import numpy

import cProfile

from jinja2 import Template
import argparse

from compariser import compare_images, compare_images_cuda, calc_difference, cuda_calc_difference


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pixel_diff_threshold', type=int, action='store', required=True,
                        help="Threshold to mark pixel as different. 0 - 255")
    parser.add_argument('--use_cuda', type=bool, action='store', required=False,
                        help="Use CUDA to compute", default=False)
    parser.add_argument('--image_diff_threshold', type=float, action='store', required=True,
                        help="Threshold to mark picture as highly changed (0-100%)")
    parser.add_argument('--base_dir', action='store', required=True, help="Base path to directory")
    parser.add_argument('--master_dir', action='store', required=True,
                        help="Directory with master-images (relative to base dir)")
    parser.add_argument('--input_dir', action='store', required=True,
                        help="Directory with new images (relative to base dir)")
    parser.add_argument('--result_dir', action='store', required=True,
                        help="Directory to save images result (relative to base dir)")
    parser.add_argument('--diff_round_signs', type=int, action='store', required=False,
                        help="Count of signs after point in difference", default=4)
    return parser.parse_args()


class Result:
    def __init__(self, name):
        self.name = name
        self.rows = []


class Row:
    def __init__(self, screen_name, difference, new_screen, diff_screen, old_screen):
        self.screen_name = screen_name
        self.difference = difference
        self.new_screen = new_screen
        self.diff_screen = diff_screen
        self.old_screen = old_screen


class AdditionalData:
    def __init__(self, filename):
        self.filename = filename
        self.warnings = 0
        self.pictures = 0


def recursive_read_files(results, prefix=""):
    options = parse_args()
    data = Result(prefix)
    for filename in os.listdir(os.path.join(options.base_dir, options.input_dir, prefix)):
        master_image = os.path.join(options.base_dir, options.master_dir, prefix, filename)

        if not os.path.exists(os.path.join(options.base_dir, options.result_dir, prefix)):
            os.mkdir(os.path.join(options.base_dir, options.result_dir, prefix))

        if os.path.isfile(master_image):
            input_image = os.path.join(options.base_dir, options.input_dir, prefix, filename)
            start_time = time.time()
            if options.use_cuda:
                result = compare_images_cuda(master_image, input_image, options.pixel_diff_threshold)
                result_image = Image.fromarray(numpy.uint8(result), 'RGB')
                result_difference = round(cuda_calc_difference(result_image), options.diff_round_signs)
                print("--- %s seconds ---" % (time.time() - start_time))
            else:
                result_image = compare_images(master_image, input_image, options.pixel_diff_threshold)
                result_difference = round(calc_difference(result_image), options.diff_round_signs)
                print("--- %s seconds ---" % (time.time() - start_time))
            result_image.save(os.path.join(options.base_dir, options.result_dir, prefix, filename))

            data.rows.append(Row(filename, result_difference, os.path.join(options.input_dir, prefix, filename),
                                 os.path.join(options.result_dir, prefix, filename),
                                 os.path.join(options.master_dir, prefix, filename)))
        elif os.path.isdir(master_image):
            new_prefix = os.path.join(prefix, filename)
            recursive_read_files(results, new_prefix)
    if len(data.rows) > 0:
        results.append(data)



def main():
    options = parse_args()

    results = []
    current_time = datetime.datetime.now()
    warnings = [Result("Warnings")]
    additional = []

    for name in os.listdir(os.path.join(options.base_dir, options.input_dir)):
        results.clear()
        additional_data = AdditionalData(name)
        additional.append(additional_data)
        recursive_read_files(results, name)

        with open("page.html", "r") as f:
            template = Template(f.read())
        with open(os.path.join(options.base_dir, name + ".html"), "w") as res_file:
            res_file.write(template.render(result_data=results, time=str(current_time)))

        for folder in results:
            for screen in folder.rows:
                additional_data.pictures += 1
                if screen.difference < options.image_diff_threshold:
                    continue
                additional_data.warnings += 1
                warning = deepcopy(screen)
                report = os.path.join(options.base_dir, name + ".html")
                warning.screen_name = f'<a href="{report}" target="_blank"><img src="{screen.screen_name}" class="img-fluid" alt="{screen.screen_name}"/></a> '
                warnings[0].rows.append(warning)

    with open(os.path.join(options.base_dir, "_Summary.html"), "w") as res_file:
        res_file.write(template.render(result_data=warnings, time=str(current_time), additional_data=additional))

    copyfile("bootstrap.min.css", os.path.join(options.base_dir, "bootstrap.min.css"))


if __name__ == "__main__":
    main()
