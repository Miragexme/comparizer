from urllib import request
import json
from progress.bar import IncrementalBar

import numpy
from numba import cuda

from PIL import Image


def test():
    image = "C:\\Users\\d.galamaga\\Downloads\\hmap00_fixed.png"
    image2 = "C:\\Users\\d.galamaga\\Downloads\\hmap00.png"

    compare_images(image, image2).save("images\\result.jpg")


def normalize(number, pixelcount):
    return 100 / (255 * pixelcount) * number


def compare_images_cuda(url1, url2, difference_threshold=10):
    image = Image.open(url1)  # Открываем изображение
    image2 = Image.open(url2)  # Открываем изображение

    numpyimg = numpy.asarray(image, dtype='int64')
    numpyimg2 = numpy.asarray(image2, dtype='int64')

    @cuda.jit
    def compare(data, difference_threshold, image, image2):

        # Thread id in a 1D block
        tx = cuda.threadIdx.x
        # Block id in a 1D grid
        ty = cuda.blockIdx.x
        # Block width, i.e. number of threads per block
        bw = cuda.blockDim.x
        # Compute flattened index inside the array
        pos = tx + ty * bw
        posx = pos % image.shape[0]
        posy = int(pos / image.shape[0])

        r1 = image[posx, posy][0]
        g1 = image[posx, posy][1]
        b1 = image[posx, posy][2]

        r2 = image2[posx, posy][0]
        g2 = image2[posx, posy][1]
        b2 = image2[posx, posy][2]

        r = r1 - r2
        if r < 0:
            r = r * -1

        g = g1 - g2
        if g < 0:
            g = g * -1

        b = b1 - b2
        if b < 0:
            b = b * -1

        val = (r + g + b) / 3
        if val < difference_threshold:
            val = 0
        else:
            val = 250

        data[posx, posy][0] = val
        data[posx, posy][1] = val
        data[posx, posy][2] = val

    threadsperblock = 64
    blockspergrid = ((numpyimg.shape[0] * numpyimg.shape[1] + (threadsperblock - 1)) // threadsperblock)

    d_numpyimg = cuda.to_device(numpyimg)
    d_numpyimg2 = cuda.to_device(numpyimg2)

    res = numpyimg2
    d_res = cuda.to_device(res)
    compare[blockspergrid, threadsperblock](d_res, difference_threshold, d_numpyimg, d_numpyimg2)

    stream = cuda.stream()
    hary = d_res.copy_to_host(stream=stream)
    return hary


def compare_images(url1, url2, difference_threshold=10):
    image = Image.open(url1)  # Открываем изображение
    image2 = Image.open(url2)  # Открываем изображение
    width = image.size[0]  # Определяем ширину
    height = image.size[1]  # Определяем высоту
    pix = image.load()  # Выгружаем значения пикселей

    width2 = image2.size[0]  # Определяем ширину
    height2 = image2.size[1]  # Определяем высоту
    pix2 = image2.load()  # Выгружаем значения пикселей

    if width != width2 or height != height2:
        print("Pictures size does not match")
        return

    result_image = Image.new("RGB", (width, height))
    result_pix = result_image.load()

    for x in range(width):
        for y in range(height):
            r, g, b = (pix[x, y][0], pix[x, y][1], pix[x, y][2])
            r2, g2, b2 = (pix2[x, y][0], pix2[x, y][1], pix2[x, y][2])

            difference = 255 if (int(abs(r - r2) + abs(g - g2) + abs(b - b2)) / 3) > difference_threshold else 0
            result_pix[x, y] = (difference, difference, difference)

    return result_image


def cuda_calc_difference(image):
    numpyimg = numpy.asarray(image, dtype='int64')

    threadsperblock = 64
    blockspergrid = ((numpyimg.shape[0] + (threadsperblock - 1)) // threadsperblock)

    @cuda.jit
    def cuda_calc(image, res):
        # Thread id in a 1D block
        tx = cuda.threadIdx.x
        # Block id in a 1D grid
        ty = cuda.blockIdx.x
        # Block width, i.e. number of threads per block
        bw = cuda.blockDim.x
        # Compute flattened index inside the array
        pos = tx + ty * bw
        # pos = 0
        r = 0
        if len(image) <= pos or len(res) <= pos:
            return
        for pix in image[pos]:
            r = r + pix[0]
        res[pos] = r

    result = numpy.zeros([numpyimg.shape[0]], dtype="int64")
    d_numpyimg = cuda.to_device(numpyimg)
    d_res = cuda.to_device(result)
    cuda_calc[threadsperblock, blockspergrid](d_numpyimg, d_res)
    # cuda_calc(d_numpyimg, d_res)
    d_res.copy_to_host(result)
    return normalize(numpy.sum(result), numpyimg.shape[0] * numpyimg.shape[1])


def calc_difference(image):
    pix = image.load()
    width = image.size[0]  # Определяем ширину
    height = image.size[1]  # Определяем высоту

    result = 0
    for x in range(width):
        for y in range(height):
            result += pix[x, y][0]
    return normalize(result, width * height)


if __name__ == "__main__":
    rawdata = request.urlopen('http://192.168.31.145:8185/channels/json')
    byteData = rawdata.read()

    data = byteData.decode("utf8")
    rawdata.close()

    jsonData = json.loads(data)

    ignores = [
        "F79F4A92",
        "790E8DBF",
        "BE9E672C",
        "2CAB2705",
        "93A57784",
        "0009D085",
        "9AE3A1F1"
    ]

    bar = IncrementalBar('Proces', max=len(jsonData['channels']))

    for channel in jsonData['channels']:
        id = channel["id"]
        if id in ignores:
            continue
        print(request.urlopen(f'http://192.168.31.145:8185/channel/{id}/rem'))
        bar.next()
    bar.finish()
